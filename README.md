# CarCar

Team:

* Jonathan - Service Microservice
* Laurie - Sales Microservice

## About CarCar

CarCar is an application that manages an automobile dealership, its inventory, services, and sales.
It consists of the following three microservices:

- Inventory
>Manages a dealership's inventory. The API consists of manufacturers, vehicle models and automobiles.
- Services
>Manages a dealership's service department. The API keeps track of service appointments for automobiles and their owners.
- Sales
>Manages a dealserhip's sales department. The API tracks automobile sales from the inventory. It has the capability to see the current inventory while also keeping track of cars that have been sold.

## Table of Contents
1. [Quick Start](#quickstart)
    1. [Steps to get our application running](#setup_steps)
2. [Design](#design)
3. [Webpage Navigation](#webpage_navigation)
4. [Inventory microservice](#inventory)
5. [Service microservice](#service)
6. [Sales microservice](#sales)

## QuickStart <a name="quickstart"></a>
Here are some quick tips and information to get CarCar up and running quickly!

Tools That We Use to Run our App:
- Docker Application
- Insomnia
- Browser
- VS Code
- CLI

Languages/Frameworks/Databases We Used:
- Python / Django Framework
- Javascript / React
- Node
- PostGresSQL / Database
- Docker
- Bootstrap


### Steps to get our application running <a name="setup_steps"></a>
> Using a browser and your machine's CLI complete the following steps
1. Fork our repository from GitHub here: https://gitlab.com/jduriu/project-beta.git
    - To fork the repository, click on the following:
        - ![Fork indicator](/ReadMeImages/gitlab_fork_image.png "gitlab forking")

2. To clone the repository, navigate to your desired directory on your local machine and use the following command `git clone <your-url-here>`
    - ![Clone indicator](/ReadMeImages/gitlab_clone_Image.png "gitlab cloning")
2. Using Docker, create a PostGres Database `docker volume create pgdata`
3. Build the Docker Images `docker-compose build`
4. Build and Run the Docker Containers `docker-compose up`
5. Make migrations to access each terminal `docker exec -it <<container name here>> bash` `python manage.py makemigrations` `python manage.py migrate`

At this point all containers should be running at the same time! If not, please repeat the above steps.
If all containers are running then the application has started and the following microservices can be used by both an API Client such as Insomnia or a Browser such as Google Chrome.


## Design <a name="design"></a>

Here is a link to our Miro whiteboard which shows our overall Design Domain Structure! This application consists of a combination of microservices!

https://miro.com/app/board/uXjVPKCpnUE=/?share_link_id=765041423215

Continue reading to learn more about the individual microservices and how they work.

## Webpage Navigation <a name="webpage_navigation"></a>

- Homepage - http://localhost:3000/
- Inventory
    - List Views
        - Manufacturers - http://localhost:3000/manufacturers/
        - Vehicle Models - http://localhost:3000/models/
        - Inventory Automobiles - http://localhost:3000/automobiles/
    - Form Views
        - Manufacturer - http://localhost:3000/manufacturers/new/
        - Vehicle Models - http://localhost:3000/models/new/
        - Inventory Automobiles - http://localhost:3000/automobiles/new/
- Services
    - List Views
        - Service Appointments - http://localhost:3000/appointments/
        - Service History Search Tool - http://localhost:3000/service_history_search/
    - Form Views
        - Technician - http://localhost:3000/technician/new/
        - Service Appointment - http://localhost:3000/appointments/new/
- Sales
    - List Views
        - Sales Records - http://localhost:3000/sale_record/
        - Sales History Tool - http://localhost:3000/sales_history/
    - Form Views
        - Sales Person - http://localhost:3000/sales_person/new/
        - Potential Customer - http://localhost:3000/potential_customer/new/
        - Sale Record  - http://localhost:3000/sale_record/new/

## Inventory Microservice <a name="inventory"></a>

Are you a dealership struggling to manage your inventory? Well then our Inventory Microservice is for you! It has the ability to create various manufacturers, vehicle models, and specific automobile instances. Our prebuild React interface allows a user friendly and quick way to add and manage all things related to an automobile dealership inventory.

### Inventory Models

Manufacturer
> - name: A text property specifying the name of the company that manufactures the automobile

VehicleModel
> - name: A text property specifying the name of the model of the vehicle
> - picture_url: A url property which enables a user to enter an image url to associate with the vehicle model object
> - manufacturer: A reference property to the manufacturer

Automobile
> - color: A text property relating to the color of the automobile
> - year: An integer property relating to the year of the automobile
> - vin: A text property relating to the vin number of the automobile can be a combination of numbers and letters. Max length is 17 characters.
> - model: A reference property to the vehicle model

Here are the REST endpoints that can be utilized with an API client such as Insonia. Along with some sample request body's for relevant endpoints.

The url for the Inventory API is http://localhost:8100

### Inventory Quick Start

***Manufacturer Endpoints***

| Request Method  | Url                     | What it does                              |
| ----------------|:-----------------------:| :----------------------------------------:|
| GET             | /api/manufacturers/     | Obtains a list of manufacturers           |
| POST            | /api/manufacturers/     | Creates a new manufacturer instance       |
| GET             | /api/manufacturers/id#  | Obtains individual manufacturer details   |
| PUT             | /api/manufacturers/id#  | Updates a specific manufacturer instance  |
| DELETE          | /api/manufacturers/id#  | Deletes an instance of a manufacturer     |

Example POST request using a JSON body to create a manufacturer:
```
{
  "name": "Chrysler"
}
```
***VehicleModel Endpoints***

| Request Method  | Url                     | What it does                              |
| ----------------|:-----------------------:| :----------------------------------------:|
| GET             | /api/models/            | Obtains a list of vehicle models          |
| POST            | /api/models/            | Creates a new vehicle model instance      |
| GET             | /api/models/id#         | Obtains individual vehicle model details  |
| PUT             | /api/models/id#         | Updates a specific vehicle model instance |
| DELETE          | /api/models/id#         | Deletes an instance of a vehicle model    |

Example POST request using a JSON body to create a vehicle model instance:
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 2
}
```

***Typical Workflow***

>* There is a provided form page to create a manufacturer which can be accessed using the navbar link "Create Manufacturer"
>* View the manufacturers list using the navbar link "Manufacturers List".
>* Create a vehicle model through a form which can be accessed using the navbar link "Create Vehicle Model"
>* View the vehicle models list using the navbar link "Vehicle Model List"
>* Create an automobile through a form which can be accessed using the navbar link "Create Automobile"
>* View the automobile inventory list using the navbar link "Automobile Inventory"

***Automobile Endpoints***

| Request Method  | Url                     | What it does                              |
| ----------------|:-----------------------:| :----------------------------------------:|
| GET             | /api/automobiles/       | Obtains a list of automobiles.            |
| POST            | /api/automobiles/       | Creates a new automobile instance         |
| GET             | /api/automobiles/vin#/  | Obtains individual automobile details     |
| PUT             | /api/automobiles/vin#/  | Updates a specific automobile instance    |
| DELETE          | /api/automobiles/vin#/  | Deletes an instance of an automobile      |

Example POST request using a JSON body to create an automobile instance:
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```


## Service microservice <a name="service"></a>

Need an appointment for an overdue oil change for your car? The CarCar automobile service microservice can do that! The CarCar automobile service microservice allows you to register a technician, create and view service appointments and see your past service history. What are you waiting for? Make your appointment today.


### Services Models

Technician
> - name: A text property specifying the name of the technician.
> - employee_number: An integer property relating to the employee number of the technician

ServiceAppointment
> - automobile: A reference property to the automobile value object model (aka AutomobileVO). This is pulled from the inventory API where the automobiles are stored.
> - owner: A text property relating to the owner of the automobile
> - date: A datetime field property specifying the date of the service appointment.
> - time: A time field property specifying the time of the service appointment.
> - technician: A reference to the technician model.
> - service: A text property relating to the service that is being provided.
> - completed: A boolean property that determines where the service appointment was completed or not.

AutomobileVO

> - vin: A text property relating to the vin number of the automobile.
> - import_href: A property which indicates where in the Inventory API, the model instance came from, if applicable
> - color: A text property relating to the color of the automobile
> - year: An integer property relating to the year of the automobile.
> - vip: A boolean property that determinse whether the customer is a vip or not.

>The AutomobileVO model is a representation of the automobile model. The preinstalled poller calls every minute to check for any new Automobile model instances in the Inventory and adds them to the Service API as an AutomobileVO model instance. While the AutomobileVO does poll and collect from the Inventory API, it is not exclusively linked. If a vehicle comes into the Service API with a vin number not associated with an existing instance from the Inventory API, then a new AutomobileVO model instance is created.

### Services Quick Start

Here are the REST endpoints that can be utilized with an API client such as Insomnia. Along with some sample request body's for relevant endpoints.

The url for the homepage is http://localhost:3000

***Technician Endpoints***

| Request Method  | Url                    | What it does                             |
| ----------------|:----------------------:| :---------------------------------------:|
| GET             | /api/technicians/      | Obtains a list of technicians            |
| POST            | /api/technicians/      | Creates a new technician instance        |
| GET             | /api/technicians/#id   | Obtains invidividual technician details  |
| PUT             | /api/technicians/#id   | Updates a specific technician instance   |
| DELETE          | /api/technicians/#id   | Deletes an instance of a technician      |

Example POST request using a JSON body to create an technician instance:
```
{
	"name": "Tim",
	"employee_number": "001"
}
```

***AutomobileVO Endpoints***

| Request Method  | Url                     | What it does                               |
| ----------------|:-----------------------:| :-----------------------------------------:|
| GET             | /api/automobileVO/      | Obtains a list of automobiles              |
| POST            | /api/automobileVO/      | Creates a new AutomobileVO instance        |
| GET             | /api/automobileVO/#id   | Obtains invidividual AutomobileVO details  |
| PUT             | /api/automobileVO/#id   | Updates a specific automobileVO instance   |
| DELETE          | /api/automobileVO/#id   | Deletes an instance of an automobileVO     |

Example POST request using a JSON body to create an service appointment instance:
```
{
	"vin": "1C3CC5FB2AN120174",
}
```

***ServiceAppointments Endpoints***

| Request Method  | Url                            | What it does                                  |
| ----------------|:------------------------------:| :--------------------------------------------:|
| GET             | /api/appointments/             | Obtains a list of service appointments        |
| GET             | /api/appointments/current      | Obtains a list of current service appointments|
| POST            | /api/appointments/             | Creates a new service appointment instance    |
| GET             | /api/appointments/id#          | Obtains individual service appointment details|
| PUT             | /api/appointments/id#          | Updates a specific appointment instance       |
| DELETE          | /api/appointments/id#          | Deletes an instance of a service appointment  |

Example POST request using a JSON body to create an service appointment instance:
```
{
	"vin": "1C3CC5FB2AN120174",
	"owner": "Tim",
	"date": "2022-10-26",
	"time": "08:00",
	"technician": "1",
	"service": "Oil Change"
}
```

***Typical Workflow***

>* There is a provided form page to create a technician which can be accessed using the navbar link "Register a technician"
>* Create a service appointment through a form which can be accessed using the navbar link "Book an Appointment"
>* View current service appointments using the navbar link "Current Service Appointments".
>* Service appointments can be canceled from the appointment list page by pressing on the "Cancel" button or marked as completed by pressing on the "Complete" button located on each row of the table.
>* View a particular vehicle's service history using the Service History Search Tool accessed using the navbar link "Service History Search Tool".
>* Enter a vin number and click the "Search" button to see a list of that vehicle's service history with the service department



## Sales microservice <a name="sales"></a>

Are you looking to buy a new car? The sales microservice with our wonderful sales representatives can help you with that! The sales microservice keeps track of the automobile sales that come from the inventory allowing you to choose from the many cars available in our inventory for you to buy. Pick our your new car today.


### Sales Models

Sales Person
> - name: A text property specifying the name of the sales person.
> - employee_number: An integer property relating to the employee number of the sales person

Potential Customer
> - name: A text property specifying the name of the customer.
> - address: A text property specifying the address of the customer.
> - phone_number: A text property specifying the phone number of the customer.

Sale Record
> - automobile: A reference property to the automobile value object model (aka AutomobileVO). This is pulled from the inventory API where the automobiles are stored.
> - potential_customer: A reference property to the specific customer object.
> - sales_person: A reference property to the specific sales person object.
> - sales_price: A text property specifying the price of the sale.

AutomobileVO
> - vin: A text property relating to the vin number of the automobile.
> - color: A text property relating to the color of the automobile.
> - year: An integer property relating to the year of the automobile.
> - sold: A boolean property determining whether the automobile has been sold or not.

The AutomobileVO model is a representation of the automobile model. A poller is used to obtain data from the inventory. This poller calls every minute to check for any new automobiles and adds them to the automobile api model as automobile value objects. The automobile value objects can be used as a reference for creating automobile instances. A person cannot see a car that is not in the inventory, and a person cannot sell a car that has already been sold.

### Sales Quick Start

Here are the REST endpoints that can be utilized with an API client such as Insomnia. Along with some sample request body's for relevant endpoints.

The url for the homepage is http://localhost:3000

***SalesPerson Endpoints***

| Request Method  | Url                   | What it does                            |
| ----------------|:---------------------:| :--------------------------------------:|
| GET             | /api/sales_person/    | Obtains a list of sales people          |
| POST            | /api/sales_person/    | Creates a new sales person instance     |
| GET             | /api/sales_person/id# | Obtains individual sales person details |
| PUT             | /api/sales_person/id# | Updates a specific sales person instance|
| DELETE          | /api/sales_person/id# | Deletes an instance of a sales person   |

Example POST request using a JSON body to create a sales person instance:
```
{
	"name": "Laurie",
	"employee_number": "1"
}
```

***PotentialCustomer Endpoints***

| Request Method  | Url                         | What it does                                  |
| ----------------|:---------------------------:| :--------------------------------------------:|
| GET             | /api/potential_customer/    | Obtains a list of potential customers         |
| POST            | /api/potential_customer/    | Creates a new potential customer instance     |
| GET             | /api/potential_customer/id# | Obtains individual potential customer details |
| PUT             | /api/potential_customer/id# | Updates a specific potential customer instance|
| DELETE          | /api/potential_customer/id# | Deletes an instance of a potential customer   |

Example POST request using a JSON body to create a potential customer instance:
```
{
	"name": "Luna",
	"address": "123 Cherry Road",
	"phone_number": "4167009000"
}
```

***SaleRecords Endpoints***

| Request Method  | Url                   | What it does                            |
| ----------------|:---------------------:| :--------------------------------------:|
| GET             | /api/sale_record/     | Obtains a list of sale records          |
| POST            | /api/sale_record/     | Creates a new sale record instance      |
| GET             | /api/sale_record/id#  | Obtains individual sale record details  |
| PUT             | /api/sale_record/id#  | Updates a specific sale record instance |
| DELETE          | /api/sale_record/id#  | Deletes an instance of a sale record    |

Example POST request using a JSON body to create a sales record instance:

```
{
	"automobile": "10000000000000000",
	"sales_person": "1",
	"potential_customer": "1",
    "sales_price": "5000"
}
```

***Typical Workflow***

>* There is a provided form page to create a sales representative which can be accessed using the navbar link "Create Sales Representative"
>* Create a customer through a form which can be accessed using the navbar link "Create Customer"
>* Create a sale record through a form which can be accessed using the navbar link "Create Sale Record"
>* View the sales record list using the navbar link "Sales Records list".
>* Sale records can be deleted from the sales records list page by pressing on the "Delete" buttons located on each row of the table
>* View a sales person history by using using the navbar link "Sales History Tool"
>* Use the dropdown menu to choose an available sales representative to see a list of their sales records


## Samples images
![Sample image](/ReadMeImages/image44.png "Example of the application")
![Sample docker](/ReadMeImages/image45docker.png "Example Docker desktop")
