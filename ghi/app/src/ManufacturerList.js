import React from 'react';
import { Link } from 'react-router-dom';

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers: [],
            // manufacturers: props.manufacturers,

        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            this.setState({ manufacturers: data.manufacturers });
        }
    }


    render() {
        return (
            <>
            <div className="pt-3">
                <h1>Vehicle Manufacturers</h1>
            </div>
            <div className="py-3">
                <Link to="/manufacturers/new">
                    <button className="btn btn-primary">Add a Manufacturer</button>
                </Link>
            </div>
            <table className="table">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                {this.state.manufacturers.map(manufacturer => {
                    return (
                    <tr key={manufacturer.href}>
                        <td>{manufacturer.name}</td>
                    </tr>
                    )
                })}
                </tbody>
            </table>
            </>
        )
    }
}

export default ManufacturerList;
