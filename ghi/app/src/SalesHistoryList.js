import React from 'react';
import { Link } from 'react-router-dom';

class SalesHistoryList extends React.Component {
    constructor() {
        super()
        this.state = {
            sale_record: [],
            sales_people: [],
            sales_person: "",
        };
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    }
    async handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({sales_person: value})
        const salespersonUrl = `http://localhost:8090/api/sales_person/${value}/sales_records/`
        const response = await fetch (salespersonUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({sale_record: data.sale_record})
        }
    }

async componentDidMount() {
    const url = "http://localhost:8090/api/sales_person/";
    const response = await fetch (url);
    const data = await response.json();

    if (response.ok) {
        this.setState({sales_people: data.sales_person });
    }
}

    render() {
        return (
            <>
            <div className="px-4 py-5 my-5">
                <h1 className="display-4 fw-bold text-center">Sales person history</h1>
                     <div className="mb-3">
                                <select onChange={this.handleSalesPersonChange} required id="sales_person"
                                    name="sales_person" className="form-select" value={this.state.sales_person}>
                                <option value="">Choose a sales person</option>
                                    {this.state.sales_people.map(sales_person => {
                                        return (
                                            <option key={sales_person.id} value={sales_person.id}>
                                                {sales_person.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
            </div>
            <Link to="sale_record/">
            </Link>
            <table className="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Customer</th>
                    <th>Vin Number</th>
                    <th>Sales Price</th>
                </tr>
                </thead>
                <tbody>
                {this.state.sale_record.map(sale => {
                    return (
                    <tr key={sale.id}>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.potential_customer.name}</td>
                        <td>{sale.automobile}</td>
                        <td>{sale.sales_price}</td>
                    </tr>
                    )
                })}
                </tbody>
            </table>
            </>
        )
    }
}

export default SalesHistoryList;
