import React, {useState} from 'react';

const PotentialCustomerForm = () => {

    const [formValues, setFormValues] = useState(
        {
            name: "",
            address: "",
            phone_number: "",
        }
    );
    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            name: "",
            address: "",
            phone_number: "",
        }))
    }

    const handleNameInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            name: event.target.value,
        }));
    };

    const handleAddressInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            address: event.target.value,
        }));
    };
    const handlePhoneNumberInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            phone_number: event.target.value,
        }));
    };

    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...formValues}

        const potentialCustomerUrl = 'http://localhost:8090/api/potential_customer/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(potentialCustomerUrl, fetchConfig);
            if (response.ok) {
                setSubmitted(true);
                resetForm();
            }
        } catch (error) {
            console.log("error", error);
        }

    }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                            <div className="alert alert-success">Customer created successfully!</div>
                    </div>
                    : null}
                    <h1>Create Customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameInputChange} placeholder="name" required
                                    type="text" name="name" id="name"
                                    className="form-control" value={formValues.name}/>
                                <label htmlFor="Name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handleAddressInputChange} placeholder="Address" name="address" id="address" className="form-control" value={formValues.address} />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handlePhoneNumberInputChange} placeholder="Phone Number" name="phoneNumber" id="phoneNumber" className="form-control" value={formValues.phone_number} />
                                <label htmlFor="address">Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    };


export default PotentialCustomerForm;
