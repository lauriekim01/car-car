import React, {useEffect, useState} from 'react';
import {NavLink} from 'react-router-dom';

const AppointmentList = () => {

    const handleComplete = async (event) => {
        const appointmentId = event.target.value
        const appointmentUrl = `http://localhost:8080/api/appointment/${appointmentId}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({completed: true}),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(appointmentUrl, fetchConfig);
            if (response.ok) {
                fetchAppointments();
            }
        } catch (error) {
            console.log("error", error)
        }
    }

    const handleCancel = async (event) => {
        const appointmentId = event.target.value
        const appointmentUrl = `http://localhost:8080/api/appointment/${appointmentId}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(appointmentUrl, fetchConfig);
            if (response.ok) {
                fetchAppointments();
            }
        } catch (error) {
            console.log("error", error);
        }

    }


    const [appointments, setAppointments] = useState([])
    useEffect(() => {
        fetchAppointments();
    }, []);

    async function fetchAppointments() {
        const url = 'http://localhost:8080/api/appointments/current/';
        try {
            const response = await fetch(url);
            const data = await response.json();
            setAppointments(data.service_appointments)
        } catch (error) {
            console.log("error", error);
        }
    };



    return (
        <>
        <div className="pt-5">
            <h1>Current Appointments</h1>
        </div>
        <div className="py-3">
            <NavLink to="/appointments/new">
                <button className="btn btn-primary">Create an appointment</button>
            </NavLink>
        </div>
        <table className="table">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>VIP</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.automobile.vin}</td>
                    <td>{appointment.owner}</td>
                    <td>{appointment.date}</td>
                    <td>{appointment.time}</td>
                    <td>{appointment.technician.name}</td>
                    <td>{appointment.service}</td>
                    <td>{appointment.automobile.vip ? "yes" : "no"}</td>
                    <td>
                        <button onClick={handleCancel} value={appointment.id}>Cancel</button>
                    </td>
                    <td>
                        <button onClick={handleComplete} value={appointment.id}>Complete</button>
                    </td>
                </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )
}

export default AppointmentList;
