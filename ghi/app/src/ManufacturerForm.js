import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';

const ManufacturerForm = () => {
    const [formValues, setFormValues] = useState(
        {
            name: "",
        }
    );

    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            name: "",
        }))
    }

const handleNameInputChange = (event) => {
    setSubmitted(false);
    setFormValues((entries) => ({
        ...entries,
        name: event.target.value,
    }));
};


const [submitted, setSubmitted] = useState(false);

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...formValues}

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
};
try {
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
        setSubmitted(true);
        resetForm();
    }
} catch (error) {
    console.log("error", error);
}

}



return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                        <NavLink to="/manufacturers">
                            <div className="alert alert-success">Manufacturer created successfully!</div>
                        </NavLink>
                    </div>
                    : null}
                <h1>Create Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameInputChange} placeholder="name" required
                            type="text" name="name" id="name"
                            className="form-control" value={formValues.name} />
                        <label htmlFor="Name">Name</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
)

};


export default ManufacturerForm;