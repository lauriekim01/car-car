import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <ul className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" aria-current="page">Inventory</NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="manufacturers">Manufacturers List</NavLink>
                  <NavLink className="dropdown-item" to="manufacturers/new">Create Manufacturer</NavLink>
                  <NavLink className="dropdown-item" to="models">Vehicle Model List</NavLink>
                  <NavLink className="dropdown-item" to="models/new">Create Vehicle Model</NavLink>
                  <NavLink className="dropdown-item" to="automobiles">Automobile Inventory</NavLink>
                  <NavLink className="dropdown-item" to="automobiles/new">Create Automobile</NavLink>
                </li>
              </ul>
            </ul>

            <ul className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="sale_record/">Sales Records List</NavLink>
                  <NavLink className="dropdown-item" to="sale_record/new">Create Sale Record</NavLink>
                  <NavLink className="dropdown-item" to="sales_person/new">Create Sales Representative</NavLink>
                  <NavLink className="dropdown-item" to="potential_customer/new">Create Customer</NavLink>
                  <NavLink className="dropdown-item" to="sales_history/">Sales History Tool</NavLink>
                </li>
              </ul>
            </ul>

            <ul className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Service</NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="technician/new">Register Technician</NavLink>
                  <NavLink className="dropdown-item" to="appointments">Current Service Appointments</NavLink>
                  <NavLink className="dropdown-item" to="appointments/new">Book an Appointment</NavLink>
                  <NavLink className="dropdown-item" to="service_history_search">Service History Search Tool</NavLink>
                </li>
              </ul>
            </ul>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
