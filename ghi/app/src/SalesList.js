import React from 'react';
import { Link } from 'react-router-dom';

class SalesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sale_record: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }
    async handleDelete(event) {
        const saleId = event.target.value
        const saleUrl = `http://localhost:8090/api/sale_record/${saleId}/`;

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        };
    }

async componentDidMount() {
    const url = "http://localhost:8090/api/sale_record/";
    const response = await fetch (url);
    const data = await response.json();

    if (response.ok) {
        this.setState({sale_record: data.sale_record });
    }
}


    render() {
        return (
            <>
            <Link to="new">
                <button>Create a sale</button>
            </Link>
            <table className="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>Vin Number</th>
                    <th>Sales Price</th>
                </tr>
                </thead>
                <tbody>
                {this.state.sale_record.map(sale => {
                    return (
                    <tr key={sale.id}>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.sales_person.employee_number}</td>
                        <td>{sale.potential_customer.name}</td>
                        <td>{sale.automobile}</td>
                        <td>{sale.sales_price}</td>
                        <td>
                        <button onClick={this.handleDelete} value={sale.id}>Delete</button>
                        </td>
                    </tr>
                    )
                })}
                </tbody>
            </table>
            </>
        )
    }
}

export default SalesList;
