from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

from .models import Technician, ServiceAppointment, AutomobileVO

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "vip",
        "id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class ServiceAppointmentDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "automobile",
        "owner",
        "date",
        "time",
        "technician",
        "service",
        "id",
        "completed",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
        "automobile": AutomobileVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
        except:
            return JsonResponse(
                {"message": "Employee number already exists"},
                status=400,
            )
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"})
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
        )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"})




@require_http_methods(["GET", "POST"])
def api_list_appointments(request, automobile_vo_vin=None, current=None):
    if request.method == "GET":
        if automobile_vo_vin is not None:
            all_appointments = ServiceAppointment.objects.all()
            service_appointments = []
            for appointment in all_appointments:
                if appointment.automobile.vin.lower() == automobile_vo_vin.lower():
                    service_appointments.append(appointment)
        elif current == "current":
            all_appointments = ServiceAppointment.objects.all()
            service_appointments = []
            for appointment in all_appointments:
                if not appointment.completed:
                    service_appointments.append(appointment)
        else:
            service_appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"service_appointments": service_appointments},
            encoder=ServiceAppointmentDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        vin_input = content["vin"] # Input from the search bar
        try:
            automobile = AutomobileVO.objects.get(vin=vin_input) # Check if the vin number exists in the AutomobileVOs
        except AutomobileVO.DoesNotExist:
            AutomobileVO.objects.create(vin=vin_input)
            automobile = AutomobileVO.objects.get(vin=vin_input)
        content["automobile"] = automobile # Assign the content automobile to the AutomobileVO
        del content["vin"]

        employee_number = content["technician"] # Enter an employee id number
        try:
            technician = Technician.objects.get(employee_number=employee_number) # Check if the id exists in Technicians
            content["technician"] = technician # Assign the technician to content
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Employee number does not exist"},
                status=400,
            )

        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentDetailEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_automobileVOs(request):
    if request.method == "GET":
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse(
            automobileVOs,
            encoder=AutomobileVODetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            automobileVO = AutomobileVO.objects.create(**content)
        except:
            return JsonResponse(
                {"message": "Incorrect formatting"},
                status=400,
            )
        return JsonResponse(
            automobileVO,
            encoder=AutomobileVODetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_automobileVO(request, pk):
    if request.method == "GET":
        try:
            automobileVO = AutomobileVO.objects.get(id=pk)
            return JsonResponse(
                automobileVO,
                encoder=AutomobileVODetailEncoder,
                safe=False
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse (
                {"message": "AutomobileVO instance does not exist"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = AutomobileVO.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            AutomobileVO.objects.filter(id=pk).update(**content)
            automobileVO = AutomobileVO.objects.get(id=pk)
            return JsonResponse(
                automobileVO,
                encoder=AutomobileVODetailEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "AutomobileVO instance does not exist"},
                status=400,
            )
